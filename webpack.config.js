const nodeExternals = require('webpack-node-externals');
const path = require("path");

module.exports = {
    target: "node",
    mode: "production", // production | development
    watch: true,
    externals: [nodeExternals()],
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: [ '.ts', '.js' ]
    },
};