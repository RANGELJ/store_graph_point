import express from "express";
import graphqlHTTP from "express-graphql";
import { buildSchema } from "graphql";
import fileSystem from "fs";
import session from "express-session";
import { buildNewContext } from "./GraphContext";
import { buildRootValue } from "./GraphRoot";

const app = express();

fileSystem.readFile("./src/schema.graphql", "utf8", (error, schemaDefinition) => {
    if (error) { throw error; }

    app.use(session({
        secret: 'const a = there is no secret',
        resave: false,
        saveUninitialized: true,
        cookie: {
            // secure: true,
            maxAge: 60000
        }
    }));

    app.use("/graph", graphqlHTTP(request => ({
        schema: buildSchema(schemaDefinition),
        rootValue: buildRootValue(),
        graphiql: true,
        context: buildNewContext(request),
    }) ));

    app.listen(5151, () => {
        console.log("Graphql client running on port 5151...");
    });

});
