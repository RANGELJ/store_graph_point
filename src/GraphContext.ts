import express from "express";
import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

export type GraphContext = {
    storeApi: {
        post(url: string, data?: any, config?: AxiosRequestConfig): Promise<ApiResponse>,
        get(url: string, config?: AxiosRequestConfig): Promise<ApiResponse>,
    },
    request: express.Request,
    isLogged: () => boolean,
};

type ApiResponse = {
    success: boolean,
    data: any,
};

export const buildNewContext = function(request: express.Request): GraphContext {

    const storeAPIInstance = axios.create({
        baseURL: "http://localhost/storeapp/",
        timeout: 3000,
        headers: { Accept: "application/json" }
    });

    return {
        storeApi: {
            async post(url: string, data?: any, config: AxiosRequestConfig = {}) {
                try {
                    if (request.session.authToken) {
                        config.headers = {
                            ...config.headers,
                            "Authorization": "Bearer " + request.session.authToken,
                        };
                    }
                    const response = await storeAPIInstance.post(url, data, config);
                    return {
                        success: true,
                        data: response.data.data,
                    };
                } catch (error) {
                    return {
                        success: false,
                        data: error.response.data,
                    };
                }
            },
            async get(url: string, config: AxiosRequestConfig = {}) {
                if (request.session.authToken) {
                    config.headers = {
                        ...config.headers,
                        "Authorization": "Bearer " + request.session.authToken,
                    };
                }
                try {
                    const response = await storeAPIInstance.get(url, config);
                    return {
                        success: true,
                        data: response.data.data,
                    };
                } catch (error) {
                    return {
                        success: false,
                        data: error.response.data,
                    };
                }
            },
        },
        request,
        isLogged() {
            return !!request.session.authToken;
        },
    };
}
