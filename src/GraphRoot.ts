import { GraphContext } from "./GraphContext";

export const buildRootValue = function() {
    return {
        async login(params: any, context: GraphContext, info: any) {
            if (context.isLogged()) {
                return false;
            }

            const response = await context.storeApi.post("user/login", {
                email: params.user,
                password: params.password,
            });

            if (response.success) {
                context.request.session.authToken = response.data.token;
            }

            return response.success;
        },

        logout(params: any, context: GraphContext, info: any) {
            if (! context.request.session.authToken) {
                return false;
            }
            context.request.session.authToken = undefined;
            return true;
        },

        async get_branchstores(params: any, context: GraphContext, info: any) {
            if (! context.isLogged()) {
                throw new Error("You are not logged in");
            }

            const response = await context.storeApi.get("admin/branchstores/get", {
                params: {
                    name: params.name,
                },
            });

            if (response.success) {
                return response.data;
            } else {
                throw new Error(response.data.message);
            }
        },

    };
};
